--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE USER monitoring;
--
-- Name: database_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE database_stats (
    id integer NOT NULL,
    host_id integer,
    snap_id integer,
    datname character varying,
    numbackends integer,
    xact_commit bigint,
    xact_rollback bigint,
    blks_read bigint,
    blks_hit bigint,
    tup_returned bigint,
    tup_fetched bigint,
    tup_inserted bigint,
    tup_updated bigint,
    tup_deleted bigint
);


ALTER TABLE public.database_stats OWNER TO postgres;

--
-- Name: dbversions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dbversions (
    id integer NOT NULL,
    snap_id bigint,
    host_id integer,
    databasename text,
    schemaversion text,
    schemadate timestamp with time zone,
    in_sync text DEFAULT 'NA'::text
);


ALTER TABLE public.dbversions OWNER TO postgres;

--
-- Name: dbversions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dbversions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.dbversions_id_seq OWNER TO postgres;

--
-- Name: dbversions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dbversions_id_seq OWNED BY dbversions.id;


--
-- Name: disk_space; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE disk_space (
    id integer NOT NULL,
    host_id integer,
    snap_id integer,
    used bigint,
    available bigint,
    usepercent integer,
    mount character varying(20)
);


ALTER TABLE public.disk_space OWNER TO postgres;

--
-- Name: disk_space_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disk_space_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.disk_space_id_seq OWNER TO postgres;

--
-- Name: disk_space_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disk_space_id_seq OWNED BY disk_space.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: scottm; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    group_cd character varying(4),
    group_name character varying
);


ALTER TABLE public.groups OWNER TO scottm;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: scottm
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO scottm;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: scottm
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: hosts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hosts (
    id integer NOT NULL,
    hostname character varying(20),
    hostip character varying(16),
    group_cd character(2),
    role_cd character varying(4),
    isinuse boolean,
    dbnames character varying,
    dbuser character varying,
    dbport integer
);


ALTER TABLE public.hosts OWNER TO postgres;

--
-- Name: hosts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hosts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hosts_id_seq OWNER TO postgres;

--
-- Name: hosts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE hosts_id_seq OWNED BY hosts.id;


--
-- Name: index_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE index_stats (
    id integer NOT NULL,
    host_id integer,
    snap_id integer,
    tablename character varying(60),
    indexname character varying,
    schemaname character varying,
    rawsize bigint,
    idx_scan bigint,
    idx_tup_read bigint,
    idx_tup_fetch bigint
);


ALTER TABLE public.index_stats OWNER TO postgres;

--
-- Name: index_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE index_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.index_stats_id_seq OWNER TO postgres;

--
-- Name: index_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE index_stats_id_seq OWNED BY index_stats.id;


--
-- Name: load_avg; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE load_avg (
    id integer NOT NULL,
    snap_id integer,
    host_id integer,
    load5 numeric,
    load10 numeric,
    load15 numeric
);


ALTER TABLE public.load_avg OWNER TO postgres;

--
-- Name: load_avg_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE load_avg_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.load_avg_id_seq OWNER TO postgres;

--
-- Name: load_avg_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE load_avg_id_seq OWNED BY load_avg.id;


--
-- Name: logs_backup_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE logs_backup_stats (
    id integer NOT NULL,
    snap_id bigint,
    host_id integer,
    backup_hostid integer,
    backup_dir character varying(300),
    backup_dir_size bigint
);


ALTER TABLE public.logs_backup_stats OWNER TO postgres;

--
-- Name: logs_backup_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE logs_backup_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.logs_backup_stats_id_seq OWNER TO postgres;

--
-- Name: logs_backup_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE logs_backup_stats_id_seq OWNED BY logs_backup_stats.id;


--
-- Name: monitoring_vars; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE monitoring_vars (
    var_name character varying(64),
    var_value character varying(100),
    host_id integer
);


ALTER TABLE public.monitoring_vars OWNER TO postgres;

--
-- Name: online_backup_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE online_backup_stats (
    id integer NOT NULL,
    snap_id bigint,
    host_id integer,
    backup_host_id integer,
    backup_dir text,
    start_ts timestamp with time zone,
    end_ts timestamp with time zone,
    backup_size bigint,
    in_progress boolean DEFAULT true,
    successful boolean DEFAULT false
);


ALTER TABLE public.online_backup_stats OWNER TO postgres;

--
-- Name: online_backup_stats_old; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE online_backup_stats_old (
    id integer NOT NULL,
    snap_id bigint,
    host_id integer,
    backup_hostid integer,
    backup_dir character varying(300),
    backup_dir_size bigint,
    backup_duration interval
);


ALTER TABLE public.online_backup_stats_old OWNER TO postgres;

--
-- Name: online_backup_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE online_backup_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.online_backup_stats_id_seq OWNER TO postgres;

--
-- Name: online_backup_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE online_backup_stats_id_seq OWNED BY online_backup_stats_old.id;


--
-- Name: online_backup_stats_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE online_backup_stats_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.online_backup_stats_id_seq1 OWNER TO postgres;

--
-- Name: online_backup_stats_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE online_backup_stats_id_seq1 OWNED BY online_backup_stats.id;


--
-- Name: ppa_reports; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ppa_reports (
    report_id integer NOT NULL,
    report_name character varying(255) NOT NULL,
    db_name character varying(255) NOT NULL,
    date_created date DEFAULT now() NOT NULL,
    created_by character varying(255) NOT NULL,
    descr text,
    report_sql text NOT NULL,
    paginate boolean NOT NULL
);


ALTER TABLE public.ppa_reports OWNER TO postgres;

--
-- Name: ppa_reports_report_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ppa_reports_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.ppa_reports_report_id_seq OWNER TO postgres;

--
-- Name: ppa_reports_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ppa_reports_report_id_seq OWNED BY ppa_reports.report_id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: scottm; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    role_cd character varying(4),
    role_name character varying,
    replication_role character varying
);


ALTER TABLE public.roles OWNER TO scottm;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: scottm
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO scottm;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: scottm
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;

--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: scottm
--

INSERT INTO roles VALUES (2, 'STPR', 'Streaming Primary', 'Primary');
INSERT INTO roles VALUES (3, 'LSPR', 'Log-Shipping Primary', 'Primary');
INSERT INTO roles VALUES (4, 'STWS', 'Streaming Warm Standby', 'Standby');
INSERT INTO roles VALUES (5, 'STHS', 'Streaming Hot Standby', 'Standby');
INSERT INTO roles VALUES (6, 'LSCS', 'Log-Shipping Cold Standby', 'Standby');
INSERT INTO roles VALUES (7, 'LSWS', 'Log-Shipping WARM Standby', 'Standby');
INSERT INTO roles VALUES (1, 'STAD', 'Stand-alone Database', 'Primary');

--
-- Name: snap_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE snap_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.snap_seq OWNER TO postgres;

--
-- Name: snapshots; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE snapshots (
    id integer NOT NULL,
    dttm timestamp without time zone,
    snapshot_complete boolean,
    metric_table character varying
);


ALTER TABLE public.snapshots OWNER TO postgres;

--
-- Name: snapshots_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE snapshots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.snapshots_id_seq OWNER TO postgres;

--
-- Name: snapshots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE snapshots_id_seq OWNED BY snapshots.id;


--
-- Name: stat_activity; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE stat_activity (
    id integer NOT NULL,
    host_id integer,
    snap_id integer,
    datid integer,
    datname character varying(60),
    procpid integer,
    usesysid integer,
    usename character varying(60),
    current_query text,
    waiting boolean,
    xact_start timestamp without time zone,
    query_start timestamp without time zone,
    backend_start timestamp without time zone,
    client_addr inet,
    client_port integer,
    snap_ts timestamp without time zone DEFAULT now(),
    db_snap_ts timestamp with time zone
);


ALTER TABLE public.stat_activity OWNER TO postgres;

--
-- Name: stat_activity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stat_activity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.stat_activity_id_seq OWNER TO postgres;

--
-- Name: stat_activity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stat_activity_id_seq OWNED BY stat_activity.id;


--
-- Name: stat_database_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stat_database_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.stat_database_id_seq OWNER TO postgres;

--
-- Name: stat_database_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stat_database_id_seq OWNED BY database_stats.id;


--
-- Name: sys_avg_cpu_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_avg_cpu_stats (
    id integer,
    host_id integer,
    snap_id integer,
    avg_cpu_user numeric,
    avg_cpu_system numeric(10,0),
    avg_cpu_idle numeric,
    avg_cpu_iowait numeric,
    avg_cpu_nice numeric,
    avg_cpu_steal numeric
);


ALTER TABLE public.sys_avg_cpu_stats OWNER TO postgres;

--
-- Name: sys_disk_io_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_disk_io_stats (
    id integer,
    host_id integer,
    snap_id integer,
    partition_name character varying(20),
    tps bigint,
    blk_reads_ps bigint,
    blk_writes_ps bigint,
    total_blk_reads bigint,
    total_blk_writes bigint
);


ALTER TABLE public.sys_disk_io_stats OWNER TO postgres;

--
-- Name: sys_vmstat; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sys_vmstat (
    id integer,
    host_id integer,
    snap_id integer,
    total_swap_mem integer,
    free_mem bigint,
    buff_mem bigint,
    cached_mem bigint,
    swapped_in bigint,
    swapped_out bigint,
    buff_blk_in bigint,
    buff_blk_out bigint
);


ALTER TABLE public.sys_vmstat OWNER TO postgres;

--
-- Name: table_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE table_stats (
    id integer NOT NULL,
    host_id integer,
    snap_id integer,
    tablename character varying(60),
    rawsize bigint,
    total_rawsize bigint,
    seq_scan bigint,
    idx_scan bigint,
    n_tup_ins bigint,
    n_tup_upd bigint,
    n_tup_del bigint,
    n_tup_hot_upd bigint,
    n_live_tup bigint,
    n_dead_tup bigint,
    last_vacuum timestamp without time zone,
    last_autovacuum timestamp without time zone,
    last_analyze timestamp without time zone,
    last_autoanalyze timestamp without time zone,
    schemaname character varying(60)
);


ALTER TABLE public.table_stats OWNER TO postgres;

--
-- Name: table_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE table_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.table_sizes_id_seq OWNER TO postgres;

--
-- Name: table_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE table_sizes_id_seq OWNED BY table_stats.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY database_stats ALTER COLUMN id SET DEFAULT nextval('stat_database_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dbversions ALTER COLUMN id SET DEFAULT nextval('dbversions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disk_space ALTER COLUMN id SET DEFAULT nextval('disk_space_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: scottm
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hosts ALTER COLUMN id SET DEFAULT nextval('hosts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY index_stats ALTER COLUMN id SET DEFAULT nextval('index_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY load_avg ALTER COLUMN id SET DEFAULT nextval('load_avg_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logs_backup_stats ALTER COLUMN id SET DEFAULT nextval('logs_backup_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats ALTER COLUMN id SET DEFAULT nextval('online_backup_stats_id_seq1'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats_old ALTER COLUMN id SET DEFAULT nextval('online_backup_stats_id_seq'::regclass);


--
-- Name: report_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ppa_reports ALTER COLUMN report_id SET DEFAULT nextval('ppa_reports_report_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: scottm
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY snapshots ALTER COLUMN id SET DEFAULT nextval('snapshots_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stat_activity ALTER COLUMN id SET DEFAULT nextval('stat_activity_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY table_stats ALTER COLUMN id SET DEFAULT nextval('table_sizes_id_seq'::regclass);


--
-- Name: dbversions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dbversions
    ADD CONSTRAINT dbversions_pkey PRIMARY KEY (id);


--
-- Name: hosts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hosts
    ADD CONSTRAINT hosts_pkey PRIMARY KEY (id);


--
-- Name: logs_backup_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY logs_backup_stats
    ADD CONSTRAINT logs_backup_stats_pkey PRIMARY KEY (id);


--
-- Name: online_backup_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY online_backup_stats_old
    ADD CONSTRAINT online_backup_stats_pkey PRIMARY KEY (id);


--
-- Name: ppa_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ppa_reports
    ADD CONSTRAINT ppa_reports_pkey PRIMARY KEY (report_id);


--
-- Name: role_cd_uniq; Type: CONSTRAINT; Schema: public; Owner: scottm; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT role_cd_uniq UNIQUE (role_cd);


--
-- Name: snapshots_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY snapshots
    ADD CONSTRAINT snapshots_pkey PRIMARY KEY (id);


--
-- Name: dv_hostid_ndx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dv_hostid_ndx ON dbversions USING btree (host_id);


--
-- Name: dv_snapid_ndx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dv_snapid_ndx ON dbversions USING btree (snap_id);


--
-- Name: idx_sa_cq; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_sa_cq ON stat_activity USING btree (current_query);


--
-- Name: idx_sa_hid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_sa_hid ON stat_activity USING btree (host_id);


--
-- Name: idx_sa_qs; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_sa_qs ON stat_activity USING btree (query_start);


--
-- Name: idx_sa_snap_ts; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_sa_snap_ts ON stat_activity USING btree (snap_ts);


--
-- Name: index_stats_hid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_stats_hid ON index_stats USING btree (host_id);


--
-- Name: index_stats_hid_sid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_stats_hid_sid ON index_stats USING btree (host_id, snap_id);


--
-- Name: la_snap_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX la_snap_id_idx ON load_avg USING btree (snap_id);


--
-- Name: sa_snapid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sa_snapid_idx ON stat_activity USING btree (snap_id);


--
-- Name: snap_dttm_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snap_dttm_idx ON snapshots USING btree (dttm);


--
-- Name: snap_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snap_id ON table_stats USING btree (snap_id);


--
-- Name: snapshots_id_metric_table_snapshot_complete; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snapshots_id_metric_table_snapshot_complete ON snapshots USING btree (id, metric_table, snapshot_complete);


--
-- Name: ts_host_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ts_host_id ON table_stats USING btree (host_id);


--
-- Name: dbversions_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dbversions
    ADD CONSTRAINT dbversions_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: dbversions_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dbversions
    ADD CONSTRAINT dbversions_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: disk_space_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disk_space
    ADD CONSTRAINT disk_space_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: disk_space_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disk_space
    ADD CONSTRAINT disk_space_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: index_stats_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY index_stats
    ADD CONSTRAINT index_stats_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: index_stats_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY index_stats
    ADD CONSTRAINT index_stats_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: load_avg_hostid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY load_avg
    ADD CONSTRAINT load_avg_hostid_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: load_avg_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY load_avg
    ADD CONSTRAINT load_avg_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: logs_backup_stats_backup_hostid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logs_backup_stats
    ADD CONSTRAINT logs_backup_stats_backup_hostid_fkey FOREIGN KEY (backup_hostid) REFERENCES hosts(id);


--
-- Name: logs_backup_stats_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logs_backup_stats
    ADD CONSTRAINT logs_backup_stats_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: logs_backup_stats_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logs_backup_stats
    ADD CONSTRAINT logs_backup_stats_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: online_backup_stats_backup_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats
    ADD CONSTRAINT online_backup_stats_backup_host_id_fkey FOREIGN KEY (backup_host_id) REFERENCES hosts(id);


--
-- Name: online_backup_stats_backup_hostid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats_old
    ADD CONSTRAINT online_backup_stats_backup_hostid_fkey FOREIGN KEY (backup_hostid) REFERENCES hosts(id);


--
-- Name: online_backup_stats_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats_old
    ADD CONSTRAINT online_backup_stats_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: online_backup_stats_host_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats
    ADD CONSTRAINT online_backup_stats_host_id_fkey1 FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: online_backup_stats_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats_old
    ADD CONSTRAINT online_backup_stats_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: online_backup_stats_snap_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY online_backup_stats
    ADD CONSTRAINT online_backup_stats_snap_id_fkey1 FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: role_cd_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hosts
    ADD CONSTRAINT role_cd_fk FOREIGN KEY (role_cd) REFERENCES roles(role_cd);


--
-- Name: stat_activity_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stat_activity
    ADD CONSTRAINT stat_activity_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: stat_activity_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stat_activity
    ADD CONSTRAINT stat_activity_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: stat_database_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY database_stats
    ADD CONSTRAINT stat_database_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: stat_database_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY database_stats
    ADD CONSTRAINT stat_database_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: table_sizes_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY table_stats
    ADD CONSTRAINT table_sizes_host_id_fkey FOREIGN KEY (host_id) REFERENCES hosts(id);


--
-- Name: table_sizes_snap_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY table_stats
    ADD CONSTRAINT table_sizes_snap_id_fkey FOREIGN KEY (snap_id) REFERENCES snapshots(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: scottm
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM scottm;
GRANT ALL ON SCHEMA public TO scottm;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT USAGE ON SCHEMA public TO monitoring;


--
-- Name: dbversions; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE dbversions FROM PUBLIC;
REVOKE ALL ON TABLE dbversions FROM postgres;
GRANT ALL ON TABLE dbversions TO postgres;


--
-- Name: disk_space; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE disk_space FROM PUBLIC;
REVOKE ALL ON TABLE disk_space FROM postgres;
GRANT ALL ON TABLE disk_space TO postgres;
GRANT SELECT ON TABLE disk_space TO monitoring;


--
-- Name: hosts; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE hosts FROM PUBLIC;
REVOKE ALL ON TABLE hosts FROM postgres;
GRANT ALL ON TABLE hosts TO postgres;
GRANT SELECT ON TABLE hosts TO nagios;
GRANT SELECT ON TABLE hosts TO monitoring;


--
-- Name: index_stats; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE index_stats FROM PUBLIC;
REVOKE ALL ON TABLE index_stats FROM postgres;
GRANT ALL ON TABLE index_stats TO postgres;
GRANT SELECT ON TABLE index_stats TO monitoring;


--
-- Name: load_avg; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE load_avg FROM PUBLIC;
REVOKE ALL ON TABLE load_avg FROM postgres;
GRANT ALL ON TABLE load_avg TO postgres;
GRANT SELECT ON TABLE load_avg TO monitoring;


--
-- Name: logs_backup_stats; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE logs_backup_stats FROM PUBLIC;
REVOKE ALL ON TABLE logs_backup_stats FROM postgres;
GRANT ALL ON TABLE logs_backup_stats TO postgres;
GRANT SELECT ON TABLE logs_backup_stats TO monitoring;


--
-- Name: monitoring_vars; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE monitoring_vars FROM PUBLIC;
REVOKE ALL ON TABLE monitoring_vars FROM postgres;
GRANT ALL ON TABLE monitoring_vars TO postgres;
GRANT SELECT,UPDATE ON TABLE monitoring_vars TO nagios;
GRANT SELECT ON TABLE monitoring_vars TO monitoring;


--
-- Name: online_backup_stats; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE online_backup_stats FROM PUBLIC;
REVOKE ALL ON TABLE online_backup_stats FROM postgres;
GRANT ALL ON TABLE online_backup_stats TO postgres;
GRANT SELECT ON TABLE online_backup_stats TO monitoring;


--
-- Name: online_backup_stats_old; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE online_backup_stats_old FROM PUBLIC;
REVOKE ALL ON TABLE online_backup_stats_old FROM postgres;
GRANT ALL ON TABLE online_backup_stats_old TO postgres;
GRANT SELECT ON TABLE online_backup_stats_old TO monitoring;


--
-- Name: ppa_reports; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE ppa_reports FROM PUBLIC;
REVOKE ALL ON TABLE ppa_reports FROM postgres;
GRANT ALL ON TABLE ppa_reports TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ppa_reports TO PUBLIC;
GRANT SELECT ON TABLE ppa_reports TO monitoring;


--
-- Name: ppa_reports_report_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE ppa_reports_report_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ppa_reports_report_id_seq FROM postgres;
GRANT ALL ON SEQUENCE ppa_reports_report_id_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE ppa_reports_report_id_seq TO PUBLIC;


--
-- Name: snapshots; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE snapshots FROM PUBLIC;
REVOKE ALL ON TABLE snapshots FROM postgres;
GRANT ALL ON TABLE snapshots TO postgres;
GRANT SELECT ON TABLE snapshots TO nagios;
GRANT SELECT ON TABLE snapshots TO monitoring;


--
-- Name: stat_activity; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE stat_activity FROM PUBLIC;
REVOKE ALL ON TABLE stat_activity FROM postgres;
GRANT ALL ON TABLE stat_activity TO postgres;
GRANT SELECT ON TABLE stat_activity TO nagios;
GRANT SELECT ON TABLE stat_activity TO monitoring;


--
-- Name: sys_avg_cpu_stats; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE sys_avg_cpu_stats FROM PUBLIC;
REVOKE ALL ON TABLE sys_avg_cpu_stats FROM postgres;
GRANT ALL ON TABLE sys_avg_cpu_stats TO postgres;
GRANT SELECT ON TABLE sys_avg_cpu_stats TO monitoring;


--
-- Name: sys_disk_io_stats; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE sys_disk_io_stats FROM PUBLIC;
REVOKE ALL ON TABLE sys_disk_io_stats FROM postgres;
GRANT ALL ON TABLE sys_disk_io_stats TO postgres;
GRANT SELECT ON TABLE sys_disk_io_stats TO monitoring;


--
-- Name: sys_vmstat; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE sys_vmstat FROM PUBLIC;
REVOKE ALL ON TABLE sys_vmstat FROM postgres;
GRANT ALL ON TABLE sys_vmstat TO postgres;
GRANT SELECT ON TABLE sys_vmstat TO monitoring;


--
-- Name: table_stats; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE table_stats FROM PUBLIC;
REVOKE ALL ON TABLE table_stats FROM postgres;
GRANT ALL ON TABLE table_stats TO postgres;
GRANT SELECT ON TABLE table_stats TO monitoring;




