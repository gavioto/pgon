#!/usr/bin/perl

# Author: Scott Mead, OpenSCG inc.
#

use POSIX;
use DBI;
use Getopt::Long;
use Config::IniFiles;
use File::Basename;

my $dirname = dirname (__FILE__);

require "$dirname/common.pl";
our $monitoringSql;

my %Config;
my $confFile;

GetOptions ("c|config=s" => \$confFile );

if ( ! $confFile ) 
{ 
    print "Please specifiy a config file with -c /path/to/config/file\n\n";
    exit(1);
}

%Config = ReadConfig($confFile);

my $semaphore=$Config{'global'}{'workingdir'} . "/tableStats.running";

sub handle_kill {
    print "Control + C caught\n";
    `rm $semaphore`;
    exit (1) 
};
$SIG{'INT'} = \&handle_kill;
$SIG{'TERM'} = \&handle_kill;

if ( -e $semaphore )
{
    print " Already running: exit\n";
    exit (1);
}

`touch $semaphore`;

my $monitoringDb= DBI->connect("dbi:Pg:host=$Config{'global'}{'monitoringdb_dbhost'} dbname=$Config{'global'}{'monitoringdb_dbname'}",$Config{'global'}{'monitoringdb_dbuser'});

#my $config = $monitoringDb->prepare("SELECT h.id, hostname, hostip,dbuser,dbport,dbnames,replication_role from hosts h, roles r where h.role_cd = r.role_cd and isinuse='t'");
my $config = $monitoringDb->prepare($monitoringSql);

# INSERT into monitoring DB
my $insertTblData="INSERT INTO table_stats "
                 ."(host_id,snap_id,tablename,schemaname,rawsize,total_rawsize,seq_scan,idx_scan,"
                 ."n_tup_ins,n_tup_upd,n_tup_hot_upd,n_tup_del,n_live_tup,n_dead_tup,"
                 ."last_vacuum,last_analyze, last_autovacuum, last_autoanalyze) "
                 ." values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
my $insert_hndl = $monitoringDb->prepare($insertTblData);

# Query to get table data 
my $getTblData="SELECT relname,schemaname,pg_relation_size(schemaname ||'.'||relname) as rawsize,"
               ."pg_total_relation_size(schemaname||'.'||relname) as total_rawsize,seq_scan, idx_scan,"
               ."n_tup_ins,n_tup_upd,n_tup_hot_upd,n_tup_del,n_live_tup,n_dead_tup,"
               ."last_vacuum,last_analyze,last_autovacuum,last_autoanalyze "
               ."from pg_stat_user_tables;";

# Setup Next snapshot
my $snap_id = OpenSnapshot($monitoringDb,'table_stats');

# Run through hosts and store info
$config->execute();
while ( my $hosts = $config->fetchrow_hashref)
{
    if ( $hosts->{'replication_role'} ne "Standby" )
    {
        my $hostId = $hosts->{'id'};
        my $hostIp = $hosts->{'hostip'};
        my $dbport = $hosts->{'dbport'};
        my $dbnames = $hosts->{'dbnames'} or 'postgres';
        my $dbuser = $hosts->{'dbuser'} or 'postgres';

        foreach my $dbName ( split( /,/,$dbnames) )
        {
            print "$hostIp ::: $dbName\n";
            my $snap_db_hndl = DBI->connect("dbi:Pg:host=$hostIp user=postgres dbname=$dbName",$dbuser);
            if ($DBI::err)
            {
                print "Connection Error : $DBI::errstr\n";
            }   
            else
            {
                my $getData= $snap_db_hndl->prepare($getTblData);
                $getData->execute();
                while ( my $row = $getData->fetchrow_hashref)
                {
                    $insert_hndl->execute($hostId, $snap_id, 
                                    $row->{'relname'},$row->{'schemaname'},$row->{'rawsize'},$row->{'total_rawsize'},
                                    $row->{'seq_scan'},$row->{'idx_scan'},$row->{'n_tup_ins'},
                                    $row->{'n_tup_upd'},$row->{'n_tup_hot_upd'},$row->{'n_tup_del'},
                                    $row->{'n_live_tup'},$row->{'n_dead_tup'},$row->{'last_vacuum'},
                                    $row->{'las_analyze'},$row->{'last_autovacuum'},$row->{'last_autoanalyze'});
                }
            }
        } 
    }     
}

CloseSnapshot($monitoringDb,$snap_id);
print "Run Complete\n";
`rm $semaphore`;


