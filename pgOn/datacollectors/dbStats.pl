#!/usr/bin/perl

# Author: Scott Mead, OpenSCG inc.

use POSIX;
use DBI;
use Getopt::Long;
use Config::IniFiles;
use File::Basename;

my $dirname = dirname (__FILE__);

require "$dirname/common.pl";
my %Config;
my $confFile;

GetOptions ("c|config=s" => \$confFile );

if ( ! $confFile ) 
{ 
    print "Please specifiy a config file with -c /path/to/config/file\n\n";
    exit(1);
}

%Config = ReadConfig($confFile);

my $semaphore=$Config{'global'}{'workingdir'} . "/current.running";

our $monitoringSql;


sub handle_kill {
    print "Control + C caught\n";
    `rm $semaphore`;
    exit (1)
};
$SIG{'INT'} = \&handle_kill;
$SIG{'TERM'} = \&handle_kill;

if ( -e $semaphore )
{
    print " Already running: exit\n";
    exit (1);
}

`touch $semaphore`;

my $monitoringDb= DBI->connect("dbi:Pg:host=$Config{'global'}{'monitoringdb_dbhost'} dbname=$Config{'global'}{'monitoringdb_dbname'}",$Config{'global'}{'monitoringdb_dbuser'});
my $config = $monitoringDb->prepare($monitoringSql );

# INSERT into monitoring DB
my $insertTblData="INSERT INTO database_stats "
                 ."(host_id,snap_id,datname,numbackends, xact_commit, xact_rollback,"
                 ."blks_read, blks_hit, tup_returned, tup_fetched, tup_inserted,"
                 ."tup_updated, tup_deleted)"
                 ." values (?,?,?,?,?,?,?,?,?,?,?,?,?)";

my $insert_hndl = $monitoringDb->prepare($insertTblData);

# Query to get table data
my $getTblData="SELECT datname,numbackends, xact_commit, xact_rollback,"
                 ."blks_read, blks_hit, tup_returned, tup_fetched, tup_inserted,"
                 ."tup_updated, tup_deleted FROM pg_stat_database;";

## Setup Next snapshot
#my $getNxtSnap = $monitoringDb->prepare("INSERT INTO snapshots (dttm,metric_table) values (now(),'database_stats') RETURNING id");
#my $closeSnap  = $monitoringDb->prepare("UPDATE snapshots SET snapshot_complete='true' WHERE id = ?");
#$getNxtSnap->execute();
#my $snap_id = ($getNxtSnap->fetchrow_hashref())->{'id'};
my $snap_id = OpenSnapshot($monitoringDb,'stat_activity');
# Run through hosts and store info
$config->execute();

while ( my $hosts = $config->fetchrow_hashref)
{
    if ( $hosts->{'role'} ne "S" )
    {
        my $hostId = $hosts->{'id'};
        my $hostIp = $hosts->{'hostip'};
        my $dbuser = $hosts->{'dbuser'};
        my $dbs = $hosts->{'dbs'};

        $dbs="postgres";
        foreach my $dbName ( split(/,/,$dbs) )
        {
            print "$hostIp ::: $dbName\n";
            my $snap_db_hndl = DBI->connect("dbi:Pg:host=$hostIp dbname=$dbName",$dbuser);
            if ($DBI::err)
            {
                print "Connection Error : $DBI::errstr\n";
            }
            else
            {
                my $getData= $snap_db_hndl->prepare($getTblData);
                $getData->execute();
                while ( my $row = $getData->fetchrow_hashref)
                {
                    $insert_hndl->execute($hostId, $snap_id,
                                    $row->{'datname'},$row->{'numbackends'},$row->{'xact_commit'},$row->{'xact_rollback'},
                                    $row->{'blks_read'},$row->{'blks_hit'},$row->{'tup_returned'},
                                    $row->{'tup_fetched'},$row->{'tup_inserted'},$row->{'tup_updated'},
                                    $row->{'tup_deleted'});
                }
            }
        }
    }
}

#$closeSnap->execute($snap_id);
CloseSnapshot($monitoringDb,$snap_id);
print "Run Complete\n";
`rm $semaphore`;