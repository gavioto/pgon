
$monitoringSql = "SELECT h.id, hostname, hostip,dbuser,dbport,dbnames,replication_role from hosts h, roles r where h.role_cd = r.role_cd and isinuse='t'";

# Sub: OpenSnapshot
# Inputs:
#       $dbh - A handle to an active database connection
#
# Single Return: Database generated snapshot identifier
#
#  This function will create a new snapshot in the snapshot database and return the 
#   integer ID number for re-use.

sub OpenSnapshot{

    my ( $dbh, $stat) = @_;

    if ( ! $dbh ) 
    { 
        print "Database connection is not valid\n";
        exit(1);
    }

    my $getNxtSnap;
	if ( $stat ) 
	{ 
        $getNxtSnap = $dbh->prepare("INSERT INTO snapshots (dttm,metric_table) values (now(),'".$stat."') RETURNING id");
	}
	else
	{
        $getNxtSnap = $dbh->prepare("INSERT INTO snapshots (dttm) values (now()) RETURNING id");
	}
    $getNxtSnap->execute();
    return ($getNxtSnap->fetchrow_hashref())->{'id'};
}

# Sub: CloseSnapshot
# Inputs:
#       $dbh - A handle to an active database connection
#
# Single Return: Database generated snapshot identifier
#
#  This function will close an open snapshot

sub CloseSnapshot{

    my ( $dbh,$snap_id ) = @_;

    if ( ! $dbh ) 
    { 
        print "Database connection is not valid\n";
        exit(1);
    }
    if ( ! $snap_id ) 
    { 
        print "Cannot close a snapshot without a snap_id\n";
        exit (1);
    }   

    my $checkSnapOpen = $dbh->prepare("SELECT snapshot_complete FROM snapshots where id=$snap_id and snapshot_complete=false");
    $checkSnapOpen->execute();
    if ( ! $checkSnapOpen ) 
    {
        print "No open snapshots\n";
        exit (1);
    }

#    my $row = $checkSnapOpen->fetchrow_hashref;
#    print "Snapshot value: ". $row->{'snapshot_complete'} . "\n";
    my $updateSnap = $dbh->prepare("UPDATE snapshots SET snapshot_complete=true where id=$snap_id");
    $updateSnap->execute();
}

# Sub: VerifyConfig
# Inputs:
#       %configHash - Hashmap representing parsed configuration file
#       
# Single Return: @MissingItems
#       Array containing a list of missing configuration entries

sub VerifyConfig { 

    my ( %configHash ) = @_;
    my @MissingItems;
    my %requiredOptions;
    
    $requiredOptions{'global'} = ['workingdir'];

    foreach my $key ( keys %requiredOptions)
    {
#        print "Checking section: $Gkey\n" 
        foreach my $val ( @{$requiredOptions{$key}} ) 
        {
#            print "\tChecking for : " . $val . " In configHash\n";
#            print "\tFound: " . $configHash{$key}{$val} . "\n";
            if ( ! defined ( $configHash{$key}{$val} ) )
            { 
#                print "\t\tIt's not there!!!\n";
                push (@MissingItems,"$key | $val");
            }
        }
    }
    return @MissingItems;
}

# Sub: ReadConfig
# Inputs: 
#       $confFile - String containing path to config file
#       $logger   - Handle to the log writer
#
# Single Return: %ConfigHash
#       A hashmap containing the parsed values of the config file
#
# Purpose:
#       This subroutine will load and return a hashmap filled with 
#       key:value pairs from the config file.
#
sub ReadConfig {

    my ($confFile) = @_;

    my %configHash;
    tie %configHash , 'Config::IniFiles',(-file => $confFile ) ;

    if ( ! %configHash ) 
    {
        print "Cannot parse configuration file, errors below:\n";
        print @Config::IniFiles::errors;
        exit (1); 
    }

    my @MissingConfig = &VerifyConfig(%configHash);

    if ( @MissingConfig ) 
    { 
        print "You have some missing config options\n";
        print "====================================\n";
        print "|    Section     |    Option       |\n";
        print "====================================\n";
        foreach my $missing ( @MissingConfig ) 
        {
            print "\t$missing\n";
        }
        print "====================================\n";
        exit (1);
    }


    return %configHash;
#    close(CONF);

}

1;

