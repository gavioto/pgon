#!/usr/bin/perl

# Author: Scott Mead, OpenSCG inc.
#

use POSIX;
use DBI;
use Getopt::Long;
use Config::IniFiles;
use File::Basename;

my $dirname = dirname (__FILE__);

require "$dirname/common.pl";

our $monitoringSql;

my %Config;
my $confFile;

GetOptions ("c|config=s" => \$confFile );

if ( ! $confFile ) 
{ 
    print "Please specifiy a config file with -c /path/to/config/file\n\n";
    exit(1);
}

%Config = ReadConfig($confFile);

my $semaphore=$Config{'global'}{'workingdir'} . "/indexStats.running";

sub handle_kill {
    print "Control + C caught\n";
    `rm $semaphore`;
    exit (1) 
};
$SIG{'INT'} = \&handle_kill;
$SIG{'TERM'} = \&handle_kill;

if ( -e $semaphore )
{
    print " Already running: exit\n";
    exit (1);
}

`touch $semaphore`;

my $monitoringDb->connect("dbi:Pg:host=$Config{'global'}{'monitoringdb_dbhost'} dbname=$Config{'global'}{'monitoringdb_dbname'}",$Config{'global'}{'monitoringdb_dbuser'});
my $config = $monitoringDb->prepare($monitoringSql);

# INSERT into monitoring DB
my $insertTblData="INSERT INTO index_stats "
                 ."(host_id,snap_id,tablename,indexname,schemaname,rawsize,idx_scan,"
                 ."idx_tup_read, idx_tup_fetch) "
                 ." values (?,?,?,?,?,?,?,?,?)";
my $insert_hndl = $monitoringDb->prepare($insertTblData);

# Query to get table data 
my $getTblData="SELECT relname,indexrelname,schemaname,pg_relation_size(schemaname ||'.'||indexrelname) as rawsize,"
             . "idx_scan, idx_tup_read, idx_tup_fetch "
             . "from pg_stat_user_indexes;";

# Setup Next snapshot
my $snap_id=OpenSnapshot($monitoringDb,'index_stats');

# Run through hosts and store info
$config->execute();

while ( my $hosts = $config->fetchrow_hashref)
{
    if ( $hosts->{'replication_role'} ne "Standby" )
    {
        my $hostId = $hosts->{'id'};
        my $hostIp = $hosts->{'hostip'};
        my $dbnames = $hosts->{'dbnames'};
        my $dbport = $hosts->{'dbport'};
        my $dbuser = $hosts->{'dbuser'} or 'postgres';

        if ( $dbnames =~ m/^$/ ) 
        {
            $dbnames='postgres';
        }

        foreach my $dbName ( split(/,/,$dbnames))
        {
            print "$hostIp ::: $dbName\n";
            my $snap_db_hndl = DBI->connect("dbi:Pg:host=$hostIp dbname=$dbName",$dbuser);
            if ($DBI::err)
            {
                print "Connection Error : $DBI::errstr\n";
            }   
            else
            {
                my $getData= $snap_db_hndl->prepare($getTblData);
                $getData->execute();
                while ( my $row = $getData->fetchrow_hashref)
                {
                    $insert_hndl->execute($hostId, $snap_id, 
                                    $row->{'relname'},$row->{'indexrelname'},$row->{'schemaname'},$row->{'rawsize'},
                                    $row->{'idx_scan'},$row->{'idx_tup_read'},$row->{'idx_tup_fetch'});
                }
            }
        } 
    }     
}

CloseSnapshot($monitoringDb,$snap_id);
print "Run Complete\n";
`rm $semaphore`;


