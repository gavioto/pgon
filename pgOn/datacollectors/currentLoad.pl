#!/usr/bin/perl


# Author: Scott Mead : OpenSCG 
# 

use POSIX;
use DBI;
use Getopt::Long;
use Config::IniFiles;
use File::Basename;

my $dirname = dirname (__FILE__);

require "$dirname/common.pl";
our $monitoringSql;

my %Config;
my $confFile;

GetOptions ("c|config=s" => \$confFile );

if ( ! $confFile ) 
{ 
    print "Please specifiy a config file with -c /path/to/config/file\n\n";
    exit(1);
}

%Config = ReadConfig($confFile);

my $semaphore=$Config{'global'}{'workingdir'} . "/current.running";

sub handle_kill {
    print "Control + C caught\n";
    `rm $semaphore`;
    exit (1) 
};

$SIG{'INT'} = \&handle_kill;
$SIG{'TERM'} = \&handle_kill;

if ( -e $semaphore )
{
    print " Already running: exit\n";
    exit (1);
}

`touch $semaphore`;

my $dbh= DBI->connect("dbi:Pg:host=$Config{'global'}{'monitoringdb_dbhost'} dbname=$Config{'global'}{'monitoringdb_dbname'}",$Config{'global'}{'monitoringdb_dbuser'});

my $config = $dbh->prepare($monitoringSql);
my $insert_load_avg= $dbh->prepare("INSERT INTO load_avg (host_id,snap_id,load5,load10,load15)values (?,?,?,?,?)");

my $getCurrentActivity="SELECT datid, datname, procpid, usesysid, usename, current_query, waiting, xact_start,"
                      ."query_start, backend_start, client_addr, client_port from pg_stat_activity";
my $saveCurrentActivity="INSERT INTO stat_activity (host_id, snap_id, datid, datname, procpid, usesysid,"
                        ."usename, current_query, waiting, xact_start, query_start, backend_start,"
                        ."client_addr, client_port) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

my $insert_stat_act= $dbh->prepare($saveCurrentActivity);

# Open a snapshot 
#my $getNxtSnap = $dbh->prepare("INSERT INTO snapshots (dttm) values (now()) RETURNING id");
#$getNxtSnap->execute();
#my $snap_id = ($getNxtSnap->fetchrow_hashref())->{'id'};
my $snap_id = OpenSnapshot($dbh,'stat_activity');

# Run through hosts and store info
$config->execute();

while ( my $hosts = $config->fetchrow_hashref)
{
    my $hostId = $hosts->{'id'};
    my $hostIp = $hosts->{'hostip'};
    my $dbport = $hosts->{'dbport'};
    my $dbuser = $hosts->{'dbuser'} or 'postgres';

    print "Saving stat data for host: $hostIp\n";

    my $avgs;
    if ( $hostIp =~ m/^\//)
    {
        $avgs=(split(/load average: /,(`uptime`)))[1];
    }
    else
    {
        $avgs=(split(/load average: /,(`ssh $hostIp uptime`)))[1];
    }
    
    my ($load5, $load10,$load15) = (split (/,/,$avgs))[0,1,2];
    $insert_load_avg->execute($hostId,$snap_id,$load5,$load10,$load10);

    if ( $hosts->{'role'} ne 'S' )
    { 
        my $snap_db_hndl = DBI->connect("dbi:Pg:dbname=postgres host=$hostIp",$dbuser);
        if ( $DBI::err ) 
        {
            print "Connection error $DBI::errstr\n";
        }
        else
        {
            my $getData=$snap_db_hndl->prepare($getCurrentActivity);
            $getData->execute();
            while (my $row = $getData->fetchrow_hashref)
            {
                $insert_stat_act->execute($hostId,$snap_id,$row->{'datid'},$row->{'datname'},$row->{'procpid'},
                                    $row->{'usesysid'},$row->{'usename'},$row->{'current_query'},$row->{'waiting'},
                                    $row->{'xact_start'},$row->{'query_start'},$row->{'backend_start'},
                                    $row->{'client_addr'},$row->{'client_port'});
            }
        }
    }
}
print "Run Complete\n";
CloseSnapshot($dbh,$snap_id);
`rm $semaphore`;


