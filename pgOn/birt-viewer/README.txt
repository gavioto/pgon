This directory corresponds to :

To make the datepicker work in eclipse, execute the below directions in:
<eclipse_install>/plugins/org.eclipse.birt.report.viewer_3.7.1.v20110905/birt/webcontent/birt

To make the datepicker work on the web, execute the below in:
<tomcat>/webapps/birt/webcontent/birt


1) Copy the 'jscal' directory to the webcontent/birt directory
2) Copy the webcontent/birt/pages/layout/FramsetFragment.jsp to the same location in eclipse (or birt-viewer)
3) Copy the webcontent/birt/pages/parameters/TextBoxParameter.jsp to the same location in eclipse (or birt-viewer)
4) To see the cool jscal datepickers in eclipse, you need to "Run->View Report->1 In Web Viewer, just using the 
    preview tab will not load the requied javascript. 
